---
title: Inicio
---

# Guías

- Seguridad
- Comunicación

# ¿Cómo obtener libertad digital?

En general, utilizando software libre, y "cultura digital".
Para una guía detallada y recomendaciones para conseguirlo,
visitar la sección de guías.

# Las [4 libertades](https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms) que se buscan cumplir

Para ser considerado software libre, debe contar con:

- Libertad 0: Libertad de ejecutar el programa.
- Libertad 1: Libertad de estudiar y cambiar el programa.
- Libertad 2: Libertad de redistribuir copias.
- Libertad 3: Libertad de distribuir copias modificadas.

Conforme a: [Free Software Foundation, Inc.](https://www.gnu.org/philosophy/free-sw.en.html)
