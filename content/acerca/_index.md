---
title: Acerca de
---

# ¿Por qué hacer esta página?

- Prevenir que criminales se beneficien de la ignorancia digital (estafas, robos).
- Los aparatos electrónicos pueden hacer la vida más fácil (automatizar, colaborar).
- Hay muchos programas/aplicaciones útiles que no son utilizados porque no son conocidos.
- Hay mucha desinformación respecto al "software libre".

"Cada nueva área, actividad de la vida, puede traer consigo
nuevos derechos humanos que son necesarios. Y los derechos
humanos dependen unos de otros. Si pierdes uno, se hace más
difícil mantener a los demás. Por lo tanto, hoy en día, la
informática es tan importante en la sociedad, que las
libertades del software libre están entre los derechos
humanos que la sociedad debe establecer y proteger." - Richard M. Stallman
